import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  padretexto="Hola soy el padre";
  textopadre2="Hola soy el padre 2";

  listacompras = {
    materiales:'Compra de alimentos',
    libros:'Libros',
    pinturas:'Pinturas',
  }

  compramaterialescolar = {
    cuadernos:'Leche, Pan, Huevo',
    boligrafo:'Condimentos, Pimienta',
    mochila:'totto,adidas'
  }

  constructor() {
  
   }

  ngOnInit(): void {
  }

}
